import { render, screen } from '@testing-library/react';
import Home from './page';


describe("Home", () => {
  it("should have Docs text", () => {
    render(<Home />)
    const myElem = screen.getByText(/Docs/i) // /.../I means that it's case insensitive.
    expect(myElem).toBeInTheDocument()
  })

  it("should contain the text 'information'", () => {
    render(<Home />)
    const myElem = screen.getByText(/information/i)
    expect(myElem).toBeInTheDocument()
  })

  it("should have a heading", () => {
    render(<Home />)
    const myElem = screen.getByRole('heading', {
      name: /Learn/i
    })
    expect(myElem).toBeInTheDocument()
  })
})
